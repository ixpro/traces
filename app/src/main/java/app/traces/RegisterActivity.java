package app.traces;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.io.File;
import java.util.Map;

import app.traces.common.BaseApplication;
import app.traces.common.Utils;
import app.traces.model.TracesModel;
import app.traces.model.User;
import app.traces.model.UsersModel;


public class RegisterActivity extends ActionBarActivity {

    Firebase tracesRef = new Firebase("https://tracesapp.firebaseio.com/traces");
    private Uri capturedImageUri;
    private ImageView capturedImageInView;
    private TextView photoUrlField;
    private TracesModel tracesModel;

    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_view);

        // Getting the global model pointer
        BaseApplication bApp = (BaseApplication) getApplication();
        tracesModel = bApp.getModel();


        final EditText nameField = (EditText) findViewById(R.id.nameField);
        final EditText emailField = (EditText) findViewById(R.id.emailField);
        final EditText passwordField = (EditText) findViewById(R.id.passwordField);
        final TextView registerMessage = (TextView) findViewById(R.id.registerMessage);

        final Button registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // validate input
                final String nameInput     = nameField.getText().toString();
                final String emailInput    = emailField.getText().toString();
                final String passwordInput = passwordField.getText().toString();
                final String photoUrlInput = photoUrlField.getText().toString();

                String[] stringList = {nameInput, emailInput, passwordInput};

                if (!Utils.isThereAnEmptyString(stringList)) {
                    // if okay, create new user into the database
                    tracesRef.createUser(emailInput, passwordInput, new Firebase.ValueResultHandler<Map<String, Object>>() {
                        @Override
                        public void onSuccess(Map<String, Object> result) {
                            // also create insert user profile into the database
                            String uid = result.get("uid").toString();
                            User newUser = new User();
                            newUser.setId(uid);
                            newUser.setName(nameInput);
                            newUser.setEmail(emailInput);
                            newUser.setPhotoUrl(photoUrlInput);
                            tracesModel.addUser(newUser);
                            Log.d("USER_CREATION_SUCCESS", "User " + uid + " created");
                        }

                        @Override
                        public void onError(FirebaseError firebaseError) {
                            Log.d("USER_CREATION_FAIL", "User creation failed");
                        }
                    });

                    // redirect back to login page
                    Intent i = new Intent(getBaseContext(), LoginActivity.class);
                    i.putExtra("USER_CREATED_MESSAGE", "User " + emailInput + " created successfully.");
                    startActivity(i);
                } else {
                    // if not, print error message
                    registerMessage.setText("Account created failed. Please check your input again !");
                }
            }
        });

        final Button backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Back to login screen
                Intent i = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(i);
            }
        });

        //Code for image picker.
        final AlertDialog dialog = giveImagePickerDialog();
        addChooseImageListener(dialog);
    }

    private AlertDialog giveImagePickerDialog() {
        final String [] items           = new String [] {"From Camera", "From SD Card"};
        ArrayAdapter<String> arrayAdapter  = new ArrayAdapter<String> (this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder     = new AlertDialog.Builder(this);
        builder.setTitle("Select Image");

        builder.setAdapter( arrayAdapter, new DialogInterface.OnClickListener() {
            public void onClick( DialogInterface dialog, int item ) {
                if (item == 0) {
                    Intent intent    = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file        = new File(Environment.getExternalStorageDirectory(),
                            "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                    capturedImageUri = Uri.fromFile(file);

                    try {
                        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, capturedImageUri);
                        intent.putExtra("return-data", true);

                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dialog.cancel();
                } else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);

                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
                }
            }
        } );

        final AlertDialog dialog = builder.create();
        return dialog;
    }

    // When
    private void addChooseImageListener(final AlertDialog dialog) {
        //TODO Ninh: put imageView and button ID here.
        capturedImageInView = (ImageView) findViewById(R.id.userImagePreview);
        photoUrlField = (TextView) findViewById(R.id.photoUrlField);

        ((Button) findViewById(R.id.uploadButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
    }

    @Override
    // When user has selected image, put that image in image view.
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        Bitmap bitmap   = null;
        String path     = "";

        if (requestCode == PICK_FROM_FILE) {
            capturedImageUri = data.getData();
            path = getRealPathFromURI(capturedImageUri); //from Gallery

            if (path == null)
                path = capturedImageUri.getPath(); //from File Manager

            if (path != null)
                bitmap  = BitmapFactory.decodeFile(path);
        } else {
            path    = capturedImageUri.getPath();
            bitmap  = BitmapFactory.decodeFile(path);
        }

        photoUrlField.setText(path);

        capturedImageInView.setImageBitmap(bitmap);
        capturedImageInView.setVisibility(View.VISIBLE);
    }

    // Returns real path of captured URI.
    public String getRealPathFromURI(Uri contentUri) {
        String [] proj      = {MediaStore.Images.Media.DATA};
        Cursor cursor       = managedQuery( contentUri, proj, null, null,null);

        if (cursor == null) return null;

        int column_index    = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }
}
