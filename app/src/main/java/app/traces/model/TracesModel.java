package app.traces.model;
import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.MutableData;
import com.firebase.client.Query;
import com.firebase.client.Transaction;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.maps.model.LatLng;
import com.grum.geocalc.Coordinate;
import com.grum.geocalc.DegreeCoordinate;
import com.grum.geocalc.EarthCalc;
import com.grum.geocalc.Point;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

public class TracesModel extends Observable {

    private static final String FIREBASE_URL = "https://tracesapp.firebaseio.com/traces";

    Firebase dbRef = new Firebase("https://tracesapp.firebaseio.com/");
    private LatLng _currentPosition = new LatLng(0,0);
    private ArrayList<Trace> _tracesAround = new ArrayList<Trace>();
    private ArrayList<Trace> _tracesByAuthor = new ArrayList<Trace>();
    private Trace _selectedTrace;

    private String userId  = "undefined";

    private static final int INITIAL_ZOOM_LEVEL = 14;
    private static final String GEO_FIRE_REF = "https://publicdata-transit.firebaseio.com/_geofire";
    private String KEY = "MODEL";

    private User _currentUser;
    private static Integer VISIBILITY_RADIUS = 600; // meters

    // default constructor
    public TracesModel() {
        //the default position
        Log.v(KEY, "Initialized");
    }

    public List<Trace> getTracesAround() {
        return _tracesAround;
    }

    public LatLng getCurrentPosition(){
        return this._currentPosition;
    }

    public void updatePosition(LatLng position) {
        if(position.latitude == _currentPosition.latitude && position.longitude == _currentPosition.longitude) return;
        _currentPosition = position;
        Log.d(KEY, "Location Changed Lat=" + _currentPosition.latitude + " Lon=" +_currentPosition.longitude);
        notifyAllObs("locationUpdated");
        updateTracesAround();
    }

    public void updateTracesAround() {

        Firebase tracesRef = dbRef.child("traces");

        //Kew
        Coordinate lat = new DegreeCoordinate(_currentPosition.latitude);
        Coordinate lng = new DegreeCoordinate(_currentPosition.longitude);
        Point kew = new Point(lat, lng);

        final Point  nw = EarthCalc.pointRadialDistance(kew, 135, 700);
        final Point  se = EarthCalc.pointRadialDistance(kew, 315, 700);

        Query queryRef = tracesRef.orderByChild("latitude")
                         .startAt(se.getLatitude())
                         .endAt(nw.getLatitude());

        // retrieve once
        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                Query secondLevel = snapshot.getRef().orderByChild("longitude")
                                                    .startAt(nw.getLongitude())
                                                    .endAt(se.getLongitude());

                secondLevel.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        // do the query once only
                        _tracesAround.clear();
                        for (DataSnapshot row : snapshot.getChildren()) {
                            Trace trace = snapToTrace(row);
                            _tracesAround.add(0,trace);
                        }
                        // once retrieved from the server notify all observers if the data is not empty;
                        if (!_tracesAround.isEmpty())
                            notifyAllObs("updatedTracesAround");
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }


    private void notifyAllObs(Object data) {
        setChanged();
        notifyObservers(data);
    }

    /*
    * Add a trace to database
    * input: userId, location, content
    * */
    public void addTrace(String content) {

        LatLng location = this._currentPosition;
        // generate created time
        // create trace object from data
        // insert into db
        // notify observers
        Firebase tracesRef = dbRef.child("traces");

        long unixTime = System.currentTimeMillis() / 1000L;
        Map<String, Object> trace = new HashMap<String, Object>();
        trace.put("author", _currentUser.getId());
        trace.put("authorName", _currentUser.getName());
        trace.put("comments", "");
        trace.put("content", content);
        trace.put("latitude", location.latitude);
        trace.put("longitude", location.longitude);
        trace.put("likes", 0);
        trace.put("dislikes", 0);
        trace.put("type", 0);
        trace.put("timestamp", unixTime);
        tracesRef.push().setValue(trace);

        Trace newTrace = new Trace();
        newTrace.setAuthorId(_currentUser.getId());
        newTrace.setAuthorName(_currentUser.getName());
        newTrace.setContent(content);
        newTrace.setLocation(location);
        newTrace.setLikes(0);
        newTrace.setDislikes(0);
        newTrace.setType(0);
        newTrace.setCreatedOn(unixTime);


        // Get the unique ID generated by push()
        String postId = tracesRef.getKey();
        newTrace.setId(postId);
        _tracesAround.add(newTrace);
        notifyAllObs("updatedTracesAround");
    }

    public void requestTracesByAuthor(String authorId) {
        // trace object for a given user

        Firebase tracesRef = dbRef.child("traces");
        Query queryRef = tracesRef.orderByChild("author").equalTo(authorId);

        // retrieve once
        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // do the query once only
                _tracesByAuthor = snapToTraceList(snapshot);
                notifyAllObs("updatedTracesByAuthor");
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    public void setSelectedTrace(String traceId){
        Firebase tracesRef = dbRef.child("traces").child(traceId);
        // retrieve once
        tracesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // do the query once only
                _selectedTrace = snapToTrace(snapshot);
                Log.v("TAG", "updated from database");
                notifyAllObs("updatedSelectedTrace");
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }
    public void addLike(String traceId){
        Firebase tracesRef = dbRef.child("traces").child(traceId).child("likes");
        tracesRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                if (currentData.getValue() == null) {
                    currentData.setValue(1);
                } else {
                    currentData.setValue((Long) currentData.getValue() + 1);
                }
                return Transaction.success(currentData); //we can also abort by calling Transaction.abort()
            }
            @Override
            public void onComplete(FirebaseError firebaseError, boolean committed, DataSnapshot currentData) {
                //This method will be called once with the results of the transaction.
                notifyAllObs("LikesUpdated");
                Log.v(KEY, "Likes updated");
            }
        });
    }
    public void addDislike(String traceId){
        Firebase tracesRef = dbRef.child("traces").child(traceId).child("dislikes");
        tracesRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                if (currentData.getValue() == null) {
                    currentData.setValue(0);
                } else {
                    currentData.setValue((Long) currentData.getValue() + 1);
                }
                return Transaction.success(currentData); //we can also abort by calling Transaction.abort()
            }
            @Override
            public void onComplete(FirebaseError firebaseError, boolean committed, DataSnapshot currentData) {
                //This method will be called once with the results of the transaction.
                notifyAllObs("DislikesUpdated");
                Log.v(KEY, "Dislikes updated");
            }
        });
    }
    public Trace getSelectedTrace(){
       return this._selectedTrace;
    }
    public ArrayList<Trace> getTracesByAuthor()
    {
        return _tracesByAuthor;
    }
    public ArrayList<Trace> snapToTraceList(DataSnapshot snapshot) {

        ArrayList<Trace> traces = new ArrayList<Trace>();

        for (DataSnapshot row : snapshot.getChildren()) {
            traces.add(0, snapToTrace(row));
        }
        return traces;
    }
    public Trace snapToTrace(DataSnapshot snap)
    {
        Trace trace = new Trace();
        trace.setId(snap.getKey());
        trace.setAuthorId(snap.child("author").getValue(String.class));

        for (DataSnapshot comment : snap.child("comments").getChildren()) {
            if (comment.getValue() != "") {
                System.out.println("KEY" + comment.getKey());
                trace.getComments().add(comment.getKey());
            }
        }

        trace.setContent(snap.child("content").getValue(String.class));
        trace.setAuthorName(snap.child("authorName").getValue(String.class));
        trace.setLikes(snap.child("likes").getValue(Integer.class));
        trace.setDislikes(snap.child("dislike").getValue(Integer.class));

        double lat = snap.child("latitude").getValue(Double.class);
        double longitude = snap.child("longitude").getValue(Double.class);

        trace.setLocation(new LatLng(lat, longitude));
        trace.setCreatedOn(snap.child("timestamp").getValue(Long.class));
        System.out.println("The read" + trace.getId());
        return trace;
    }

    public void setCurrentUser(String userId) {
        Firebase tracesRef = dbRef.child("users");
        Query queryRef = tracesRef.orderByChild("userId").equalTo(userId);
        // retrieve once
        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // do the query once only
                _currentUser = snapToUser(snapshot.getValue());
                Log.v(KEY, "current user updated from database = " + _currentUser.getName());
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    public User getCurrentUser() {
        return _currentUser;
    }

    /**
     * Insert user profile into the database
     */
    public void addUser(User user) {
        Firebase tracesRef = dbRef.child("users");
        Map<String, Object> userObj = new HashMap<String, Object>();
        userObj.put("userId",   user.getId());
        userObj.put("name",     user.getName());
        userObj.put("email",    user.getEmail());
        userObj.put("photoUrl", user.getPhotoUrl());
        userObj.put("latitude", user.getLatitude());
        userObj.put("longitude",user.getLongitude());
        tracesRef.push().setValue(userObj);
    }

    public User snapToUser(Object snap)
    {
        User user = new User();
        if(snap == null) return user;
        HashMap hm = (HashMap) snap;
        String userKey = (String)hm.keySet().toArray()[0];
        HashMap<String, Object> values = (HashMap<String, Object>) hm.values().toArray()[0];


        user.setEmail((String)values.get("email"));
        user.setPhotoUrl((String) values.get("photoUrl"));
        user.setName((String) values.get("name"));
        user.setLatitude((double) values.get("latitude"));
        user.setLongitude((double) values.get("longitude"));
        user.setId(userKey);
        return user;
    }


}