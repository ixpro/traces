package app.traces.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * User object
 */
public class User {
    private String name = "";



    private String email = "";
    private String id = "";
    private String photoUrl = "";
    private double latitude = 0;
    private double longitude = 0;


    private static double DEFAULT_LAT = 0;
    private static double DEFAULT_LON = 0;

    // Construct a user by name, email
    public User() {
        this.name = "";
        this.email = "";
        this.photoUrl = "";
    }


    public String getId() {
        return id;
    }
    public String getEmail() {
        return email;
    }
    public String getName() {

        return name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }
    public double getLatitude() {
        return latitude;
    }
    public double getLongitude() {
        return longitude;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setId(String id) {
        this.id = id;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

}
