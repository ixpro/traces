package app.traces.model;

import com.google.android.gms.maps.model.LatLng;

import android.text.format.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Trace {
    private String id;
    private String  authorId;
    private String authorName;
    private Integer likes;
    private Integer dislikes;
    private Integer type;
    private Long createdOn;
    private List<String> comments;
    private String content;
    private LatLng location;
    private String userPhotoPath;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Trace() {
        this.comments = new ArrayList<String>();
    }

    public Integer getDislikes() {
        return this.dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public Integer getLikes() {
        return likes;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Long createdOn) {
        this.createdOn = createdOn;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserPhotoPath() {
        //return this.userPhotoPath;
        // TODO: retrieve real user photo link
        return "";
    }

    // Returns date object from the created on integer.
    public Date getDate() {
        Date date = new Date();
        date.setTime((long) getCreatedOn()*1000);
        return date;
    }

    // Returns how much time has been passed since this trace was created.
    public String getTimeSinceTrace(){
        Date now = new Date();
        String timeSinceTrace = DateUtils.getRelativeTimeSpanString(getDate().getTime(), now.getTime(), DateUtils.SECOND_IN_MILLIS).toString();
        return timeSinceTrace;
    }
}