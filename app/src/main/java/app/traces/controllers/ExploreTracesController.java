package app.traces.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import app.traces.R;
import app.traces.common.BaseApplication;
import app.traces.model.TracesModel;
import app.traces.views.ExploreTracesView;

public class ExploreTracesController extends ActionBarActivity  {

    private TracesModel tracesModel;
    private ExploreTracesView exploreTracesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.explore_traces_view);

        // Getting the global model pointer
        BaseApplication bApp = (BaseApplication) getApplication();
        tracesModel = bApp.getModel();
        exploreTracesView = new ExploreTracesView(this);
        //Getting all traces around and put them in list views.
        requestAllTracesAround();
    }

    // Returns the traces of the current user.
    private void requestAllTracesAround() {
        tracesModel.updateTracesAround();
    }


    public void moveToTraceDetailView(View view) {
        Log.v("TAG", view.getTag().toString());
        //Delete the observer.
        tracesModel.deleteObserver(exploreTracesView);
        //ImageButton openTraceButton =(ImageButton) view.findViewById(R.id.openTraceButton);
        tracesModel.setSelectedTrace(view.getTag().toString());

        // Start the my traces activity
        Intent i = new Intent(getBaseContext(), TraceDetailController.class);
        startActivity(i);
    }



}
