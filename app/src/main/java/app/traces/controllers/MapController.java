package app.traces.controllers;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.List;

import app.traces.R;
import app.traces.common.BaseApplication;
import app.traces.common.GPS;
import app.traces.common.IGPSInterface;
import app.traces.common.SharedPreferenceManager;
import app.traces.common.Utils;
import app.traces.model.TracesModel;
import app.traces.views.MapView;


public class MapController extends ActionBarActivity implements IGPSInterface {

    // The map. Might be null if Google Play services APK is not available.
    private GoogleMap map;
    private GPS gps;
    private TracesModel tracesModel;
    List<Marker> markerList = new ArrayList<Marker>();
    private MapView mapView;
    private String KEY ="MapController";
    private EditText textTraceTextField;
    private InputMethodManager keyboardManager;
    private LinearLayout textTraceLayoutLayer;

    public MapController() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_view);

        // Getting the global model pointer
        BaseApplication bApp = (BaseApplication) getApplication();
        tracesModel = bApp.getModel();

        this.mapView = new MapView(this, getMap());
        initializeLocation();
        gps = new GPS(this);

        this.textTraceTextField = (EditText) findViewById(R.id.traceTextField);

        this.textTraceLayoutLayer = (LinearLayout) findViewById(R.id.textTraceLayout);
        this.keyboardManager  = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        addTextFieldListener();
        // Link trace button to manager.
        ImageButton textTraceButton = (ImageButton) findViewById(R.id.placeTraceBtn);
        textTraceButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {showTextTraceElements();}
        });

        // Link explore button to manager.
        ImageButton exploreTraceButton = (ImageButton) findViewById(R.id.exploreBtn);
        exploreTraceButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {moveToExploreTracesView();}
        });
//
        // Link trace button to manager.
        ImageButton activityTraceButton = (ImageButton) findViewById(R.id.activityBtn);
        activityTraceButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {moveToMyTracesView();}
        });
    }

    // Define what happens when the text has been entered.
    private void addTextFieldListener() {
        textTraceTextField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId != 35) return false;

                // Save text
                String textTrace = textTraceTextField.getText().toString();
                Log.v("USERINPUT", textTrace);

                // Push element to database.
                if(!Utils.isStringEmpty(textTrace)) {
                    // Get the author_id
                    String author_id = "user_id1";
                    // Get the location
                    LatLng location = new LatLng(0,0);
                    // Push element to database.
                    tracesModel.addTrace(textTrace);
                }
                // Hide elements in main views.
                hideTextTraceElements();
                return true;
            }
        });
    }

    // Shows elements for input of text traces.
    private void showTextTraceElements() {
        textTraceLayoutLayer.setVisibility(View.VISIBLE);

        textTraceTextField.setFocusableInTouchMode(true);
        textTraceTextField.requestFocus();
        // Show keyboard. TODO puts wrong keyboard up.. when clicking the right one is shown.
        keyboardManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    // Hides elements for input of text traces.
    private void hideTextTraceElements() {
        textTraceTextField.setText("");
        textTraceLayoutLayer.setVisibility(View.GONE);
        //to hide it, call the method again
        keyboardManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

    }

    private GoogleMap getMap(){

       // Do a null check to confirm that we have not already instantiated the map.
      GoogleMap map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                        .getMap();
        if (map != null) {
            // Enable MyLocation Layer of Google Map.
            map.setMyLocationEnabled(true);
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
     return map;
    }

    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Log.v("GPS", "GPS DISABLED");
    }

    public void locationChanged(double longitude, double latitude) {
        // is fired when a location is changed
//        Log.d(KEY, "UserLocationChanged EV  Longitude: " + longitude);
//        Log.d(KEY, "UserLocationChanged EV   Latitude: " + latitude);
        tracesModel.updatePosition(new LatLng(latitude, longitude));
    }

    // Initializes user location
    private void initializeLocation() {
        // Get the name of the best provider
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);

        // Get location. If it does not exist, then set a hardcoded location.
        Location userLocation = locationManager.getLastKnownLocation(provider);
        if (userLocation == null) {
            userLocation = new Location(provider);
            userLocation.setLatitude(59.3288);
            userLocation.setLongitude(18.0619);
        }
       Log.d(KEY, "Location Initialized  Lat=" + userLocation.getLatitude() + "Long=" + userLocation.getLongitude());
       tracesModel.updatePosition(new LatLng(userLocation.getLatitude(), userLocation.getLongitude()));
    }

    // After button press, moves to my traces.
    public void moveToMyTracesView() {
        //Delete the observer.
        tracesModel.deleteObserver(this.mapView);

        // Start the my traces activity
        Intent i = new Intent(this.getBaseContext(), MyTracesController.class);
        startActivity(i);
    }

    // After button press, moves to explore traces.
    public void moveToExploreTracesView() {
        //Delete the observer.
        tracesModel.deleteObserver(this.mapView);
        // Start the explore traces activity
        Intent i = new Intent(getBaseContext(), ExploreTracesController.class);
        startActivity(i);
    }

}
