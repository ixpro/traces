package app.traces.controllers;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;

import app.traces.LoginActivity;
import app.traces.R;
import app.traces.common.BaseApplication;
import app.traces.common.SharedPreferenceManager;
import app.traces.model.TracesModel;
import app.traces.views.MyTracesView;

public class MyTracesController extends ActionBarActivity  {

    private TracesModel tracesModel;
    private MyTracesView myTracesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_traces_view);

        // Getting the global model pointer
        BaseApplication bApp = (BaseApplication) getApplication();
        tracesModel = bApp.getModel();
        myTracesView = new MyTracesView(this);

        //Getting the current user traces and put them in list views.
        requestMyTraces();
    }

    // Request the model to retrieve the traces of the user
    private void requestMyTraces() {
        String userId = tracesModel.getCurrentUser().getId();
        tracesModel.requestTracesByAuthor(userId);
    }

    public void onLogout (View view){
        // Clear login session
        SharedPreferenceManager.setCurrentUser(this.getBaseContext(), "");
        // Back to login screen
        Intent i = new Intent(getBaseContext(), LoginActivity.class);
        startActivity(i);
    }
}
