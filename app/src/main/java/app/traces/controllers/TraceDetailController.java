package app.traces.controllers;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Observable;
import java.util.Observer;

import app.traces.R;
import app.traces.common.BaseApplication;
import app.traces.common.GPS;
import app.traces.model.TracesModel;
import app.traces.views.MyTracesView;
import app.traces.views.TraceDetailView;

public class TraceDetailController extends ActionBarActivity {

    private TraceDetailView traceDetailView;

    private TracesModel tracesModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trace_detail_view);

        // Getting the global model pointer
        BaseApplication bApp = (BaseApplication) getApplication();
        tracesModel = bApp.getModel();

        traceDetailView = new TraceDetailView(this);

        // Request which trace has been selected.
        requestSelectedTrace();
    }

    public void requestSelectedTrace() {

    }

    public void onLikeClicked(View view)
    {
        //tracesModel.addLike();
    }
}
