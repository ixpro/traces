package app.traces;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import app.traces.common.BaseApplication;
import app.traces.common.SharedPreferenceManager;
import app.traces.common.Utils;
import app.traces.model.TracesModel;
import app.traces.controllers.MapController;

public class LoginActivity extends ActionBarActivity {

    Firebase tracesRef = new Firebase("https://tracesapp.firebaseio.com/traces");
    private TracesModel tracesModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Go straight to main view if user logged in
        String currentUserId = SharedPreferenceManager.getCurrentUser(this.getBaseContext());

        BaseApplication bApp = (BaseApplication) getApplication();
        tracesModel = bApp.getModel();

        if (!Utils.isStringEmpty(currentUserId)) {
            tracesModel.setCurrentUser(currentUserId);
            Intent i = new Intent(getBaseContext(), MapController.class);
            startActivity(i);
        }

        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.login_view);

        final Context ctx = this.getBaseContext();

        final EditText emailField = (EditText) findViewById(R.id.emailField);
        final EditText passwordField = (EditText) findViewById(R.id.passwordField);
        final TextView loginMessage = (TextView) findViewById(R.id.loginMessage);

        // set message if exist
        Intent i = getIntent();
        String userCreatedMessage = i.getStringExtra("USER_CREATED_MESSAGE");

        if (!Utils.isStringEmpty(userCreatedMessage)) {
            loginMessage.setText(userCreatedMessage);
            loginMessage.setTextColor(Color.GREEN);
        }

        final Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click

                String emailInput = emailField.getText().toString();
                String passwordInput = passwordField.getText().toString();
                // Check credentials
                tracesRef.authWithPassword(emailInput, passwordInput, new Firebase.AuthResultHandler() {
                    @Override
                    public void onAuthenticated(AuthData authData) {
                        Log.d("TRACE_LOGIN_SUCCESS", authData.getUid());

                        // Get userId and save userId in the model.
                        String userId =  authData.getUid();
                        tracesModel.setCurrentUser(userId);
                        SharedPreferenceManager.setCurrentUser(ctx, userId);

                        // Start the main activity
                        Intent i = new Intent(getBaseContext(), MapController.class);
                        startActivity(i);
                    }

                    @Override
                    public void onAuthenticationError(FirebaseError firebaseError) {
                        Log.d("TRACE_LOGIN_FAIL", firebaseError.getMessage());

                        loginMessage.setText("Noooo ! Email & password combination is not correct !");
                    }
                });
            }
        });

        final Button registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), RegisterActivity.class);
                startActivity(i);
            }
        });
    }
}