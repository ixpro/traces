package app.traces.views;

import android.app.Activity;
import android.content.Context;
import android.widget.ListView;

import com.google.android.gms.maps.GoogleMap;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import app.traces.R;
import app.traces.common.BaseApplication;
import app.traces.model.Trace;
import app.traces.model.TracesModel;

public class MyTracesView implements Observer {

    TracesModel tracesModel;
    Activity parentActivity;

    public MyTracesView(Activity parentAct) {

        //getting the pointer to the mainView Activity
        this.parentActivity = parentAct;

        // Getting the global model pointer
        BaseApplication bApp = (BaseApplication) parentActivity.getApplication();
        tracesModel = bApp.getModel();

        // View listens for updates via observer.
        tracesModel.addObserver(this);

    }

    @Override
    // After current user traces are found
    public void update(Observable observable, Object data) {
        if (data.equals("updatedTracesByAuthor")) {
            List<Trace> myTraces = tracesModel.getTracesByAuthor();

            // Pass my traces to adapter.
            ListView listview = (ListView) parentActivity.findViewById(R.id.myTracesList);
            listview.setAdapter(new MyTracesItemAdapter(parentActivity, myTraces));
        }
    }

}
