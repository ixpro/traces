package app.traces.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

import app.traces.R;
import app.traces.model.Trace;

/**
 * TODO make this class work if we want customized info windows on the map.
 */
public class InfoWindowItemAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;
    private static LayoutInflater inflater = null;

    List<Trace> tracesAround;

    public InfoWindowItemAdapter(Context context, List<Trace> tracesAround) {
        this.context = context;
        this.tracesAround = tracesAround;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // Use default InfoWindow frame
    @Override
    public View getInfoWindow(Marker arg0) {
        return null;
    }

    // Defines the contents of the InfoWindow
    @Override
    public View getInfoContents(Marker marker) {

        // Getting view from the layout file
        View v = inflater.inflate(R.layout.map_info_window, null);

        // Getting reference to the TextViews to set contents
        TextView userName = (TextView) v.findViewById(R.id.userNameInfoWindow);
        TextView textTrace = (TextView) v.findViewById(R.id.textTraceInfoWindow);
        //TODO ImageView userImage =(ImageView)vi.findViewById(R.id.userImageInfoWindow);

        // Returning the view containing InfoWindow contents
        return v;
    }
}
