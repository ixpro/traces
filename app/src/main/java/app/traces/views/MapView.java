package app.traces.views;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import app.traces.R;
import app.traces.common.BaseApplication;
import app.traces.common.GPS;
import app.traces.model.Trace;
import app.traces.model.TracesModel;


public class MapView implements Observer {

    private TracesModel tracesModel;
    // The map. Might be null if Google Play services APK is not available.
    private GoogleMap map;
    private String KEY = "MapView";
    Activity mainView;
    // Items for GPS position and circle.
    private Circle searchCircle;
    private float zoomLevel = 15;
    List<Marker> markerList = new ArrayList<Marker>();

     // Constructor
    public MapView(Activity main, GoogleMap map) {

        //getting the pointer to the mainView Activity
        this.mainView = main;
        this.map = map;

        EditText textTraceTextField = (EditText) mainView.findViewById(R.id.traceTextField);
        textTraceTextField.setHorizontallyScrolling(false);
        textTraceTextField.setMaxLines(Integer.MAX_VALUE);

        // Getting the global model pointer
        BaseApplication bApp = (BaseApplication) main.getApplication();
        tracesModel = bApp.getModel();

        //registering as an observer of the model
        tracesModel.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        Log.v(KEY, "Update Message Received "+ data );
        if(data== "locationUpdated") {
            updateMapPosition();
            Log.d(KEY, "Location Updated Received");
        }
        if(data == "updatedTracesAround")
            fillMapWithTraces();
    }

    // Fills map with traces.
    private void fillMapWithTraces() {
        ArrayList<LatLng> tracesLocations = new ArrayList<LatLng>();
        List<Trace> traces = tracesModel.getTracesAround();

        // If markers are already there, delete them.
        if(traces.size() > 0) {
            // Empty map.
            for (Marker marker : markerList) {
                marker.remove();
            }
            markerList.clear();
        }

        for (int i = 0; i < tracesModel.getTracesAround().size(); i++) {
            Trace trace = traces.get(i);
            LatLng location = trace.getLocation();
            tracesLocations.add(location);

            putTraceMarkerOn(location, trace);
        }
    }

    // Updates the main pointer position on the map
    private void updateMapPosition()
    {

        LatLng myCoordinates = tracesModel.getCurrentPosition();
        drawLocationCircle(myCoordinates);
        // Zoom in on user location.
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(myCoordinates, zoomLevel);
        map.animateCamera(yourLocation);
    }

    private double zoomLevelToRadius(double zoomLevel) {
        // Approximation to fit circle into views
        return 16384000/Math.pow(2, zoomLevel);
    }

    // draws a location circle showing the radius of traces visibility
    private void drawLocationCircle( LatLng coordinates)
    {
        // check if it already exists on the map then change position
        if(this.searchCircle!=null){
            this.searchCircle.setCenter(coordinates);
            this.searchCircle.setRadius(zoomLevelToRadius(zoomLevel));
        }else
        //else if it is not there, create it and put on the map
        {
            this.searchCircle = this.map.addCircle(new CircleOptions().center(coordinates).radius(zoomLevelToRadius(zoomLevel)));
            this.searchCircle.setFillColor(Color.argb(65, 143, 177, 213));
            this.searchCircle.setStrokeColor(Color.argb(65, 0, 0, 0));
            this.searchCircle.setStrokeWidth(2);
        }
    }

    // Creates a marker on location location.
    private void putTraceMarkerOn(LatLng location, Trace trace) {
        String author = trace.getAuthorName();
        String text = trace.getContent();
        String shortenedText = text.substring(0, Math.min(text.length(), 20)) + "...";

        MarkerOptions options = new MarkerOptions()
                .position(location)
                .title(author)
                .snippet(shortenedText)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.traces_icon_hdpi));

        Marker  marker =  map.addMarker(options);
        markerList.add(marker);

        // Show if the current user is the trace author.
        if (author.equals(tracesModel.getCurrentUser().getId())) {
            marker.showInfoWindow();
        }
    }


}
