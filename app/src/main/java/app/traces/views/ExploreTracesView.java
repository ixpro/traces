package app.traces.views;

import android.app.Activity;
import android.content.Context;
import android.widget.ListView;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import app.traces.R;
import app.traces.common.BaseApplication;
import app.traces.model.Trace;
import app.traces.model.TracesModel;

public class ExploreTracesView implements Observer {

    TracesModel tracesModel;
    Activity parentActivity;

    public ExploreTracesView(Activity parentAct) {

        //getting the pointer to the mainView Activity
        this.parentActivity = parentAct;

        // Getting the global model pointer
        BaseApplication bApp = (BaseApplication) parentActivity.getApplication();
        tracesModel = bApp.getModel();

        // View listens for updates via observer.
        tracesModel.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        if (data.equals("updatedTracesAround")) {
            List<Trace> tracesAround = tracesModel.getTracesAround();
            // Pass my traces to adapter.
            ListView listview = (ListView) parentActivity.findViewById(R.id.exploreTracesList);
            listview.setAdapter(new ExploreTracesItemAdapter(parentActivity, tracesAround));
        }
    }

}
