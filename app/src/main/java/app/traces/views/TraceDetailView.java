package app.traces.views;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

import java.util.Observable;
import java.util.Observer;

import app.traces.R;
import app.traces.common.BaseApplication;
import app.traces.common.Utils;
import app.traces.model.Trace;
import app.traces.model.TracesModel;

public class TraceDetailView implements Observer {

    TracesModel tracesModel;
    Activity parentActivity;
    String TAG = "TraceDetailView";

    public TraceDetailView(Activity parentAct) {

        //getting the pointer to the mainView Activity
        this.parentActivity = parentAct;

        // Getting the global model pointer
        BaseApplication bApp = (BaseApplication) parentActivity.getApplication();
        tracesModel = bApp.getModel();

        // View listens for updates via observer.
        tracesModel.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        // When the selected trace has been updated in the model, move on
        if (data.equals("updatedSelectedTrace")) {
            Log.v(TAG, "updatedSelectedTrace");
            populateView();
        }
    }

    private void populateView()
    {
        // Find all fields for this item in the list.
        TextView userName = (TextView) parentActivity.findViewById(R.id.detailUserName);
        TextView dayTime  = (TextView) parentActivity.findViewById(R.id.detailDayTime);
        TextView likes    = (TextView) parentActivity.findViewById(R.id.detailLikes);
        TextView dislikes = (TextView) parentActivity.findViewById(R.id.detailDislikes);
        TextView detailTraceText = (TextView) parentActivity.findViewById(R.id.detailTraceText);
        // TODO ImageView userImage =(ImageView) parentActivity.findViewById(R.id.traceDetailUserName);

        Trace selectedTrace = tracesModel.getSelectedTrace();

        if(selectedTrace == null) {
            Log.v(TAG, "Selected Trace EMPTY");
            return;
        }

        String author = selectedTrace.getAuthorName();
        String traceContent = selectedTrace.getContent();

        String timeSince = selectedTrace.getTimeSinceTrace();
        String likesNb = selectedTrace.getLikes() + " Likes";
        String dislikesNb = Utils.checkIfNotNull(selectedTrace.getDislikes() + "") + " Dislikes";
        //  All traces fields are updated with the users traces.
        userName.setText(author);
        dayTime.setText(timeSince);
        likes.setText(likesNb);
        dislikes.setText(dislikesNb);
        detailTraceText.setText(traceContent);
    }
}
