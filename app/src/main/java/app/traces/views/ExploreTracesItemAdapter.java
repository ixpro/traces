package app.traces.views;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.traces.R;
import app.traces.common.Utils;
import app.traces.model.Trace;

public class ExploreTracesItemAdapter extends BaseAdapter {
    List<Trace> tracesAround = new ArrayList<Trace>();
    private static LayoutInflater inflater = null;
    Context context;

    public ExploreTracesItemAdapter(Context context, List<Trace> tracesAround) {
        this.context = context;
        this.tracesAround = tracesAround;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return tracesAround.size();
    }

    @Override
    public Object getItem(int position) {
        return tracesAround.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    // Here the magic happens: update rows with traces around data.
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null) {
            vi = inflater.inflate(R.layout.explore_traces_row, null);
        }

        // Find all fields for this item in the list.
        TextView userName = (TextView) vi.findViewById(R.id.exploreTracesUserName);
        TextView timeDay = (TextView) vi.findViewById(R.id.exploreTracesTimeDay);
        TextView likes = (TextView) vi.findViewById(R.id.exploreTracesLikes);
        TextView dislikes = (TextView) vi.findViewById(R.id.exploreTracesDislikes);
        ImageButton openTraceButton =(ImageButton) vi.findViewById(R.id.openTraceButton);
        // TODO ImageView userImage =(ImageView)vi.findViewById(R.id.exploreTracesUserImage);

        // Retrieve Trace properties
        String authorOfTrace = Utils.checkIfNotNull(tracesAround.get(position).getAuthorName());
        String timeSinceTrace = Utils.checkIfNotNull(tracesAround.get(position).getTimeSinceTrace());
        String numberOfLikes = Utils.checkIfNotNull(tracesAround.get(position).getLikes() + "") + " Likes";
        String numberOfDislikes = Utils.checkIfNotNull(tracesAround.get(position).getDislikes() + "") + " Dislikes";

        //  All traces fields are updated with the users traces.
        userName.setText(authorOfTrace);
        timeDay.setText(timeSinceTrace);
        likes.setText(numberOfLikes);
        dislikes.setText(numberOfDislikes);
        // TODO userImage.setImageURI(myTraces.get(position).getUserImage());

        // Set tag of button to trace id for further steps.
        openTraceButton.setTag(tracesAround.get(position).getId());

        return vi;
    }
}
