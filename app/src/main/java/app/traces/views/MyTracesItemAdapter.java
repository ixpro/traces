package app.traces.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.traces.R;
import app.traces.common.Utils;
import app.traces.model.Trace;

public class MyTracesItemAdapter extends BaseAdapter {
    List<Trace> myTraces = new ArrayList<Trace>();
    private static LayoutInflater inflater = null;
    Context context;

    public MyTracesItemAdapter(Context context, List<Trace> myTraces) {
        this.context = context;
        this.myTraces = myTraces;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return myTraces.size();
    }

    @Override
    public Object getItem(int position) {
        return myTraces.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    // Here the magic happens: update rows with my traces data.
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null) {
            vi = inflater.inflate(R.layout.my_traces_row, null);
        }

        // Find all fields for this item in the list.
        TextView timeDay = (TextView) vi.findViewById(R.id.myTracesTimeDay);
        TextView textTrace = (TextView) vi.findViewById(R.id.myTracesTextTrace);
        TextView likes = (TextView) vi.findViewById(R.id.myTracesLikes);
        TextView dislikes = (TextView) vi.findViewById(R.id.myTracesDislikes);
        ImageView userImage =(ImageView)vi.findViewById(R.id.myTracesUserImage);

        // Retrieve Trace properties
        String timeSinceTrace = Utils.checkIfNotNull(myTraces.get(position).getTimeSinceTrace());
        String textOfTrace = Utils.checkIfNotNull((myTraces.get(position).getContent()));
        String numberOfLikes = Utils.checkIfNotNull(myTraces.get(position).getLikes() + "") + " Likes";
        String numberOfDislikes = Utils.checkIfNotNull(myTraces.get(position).getDislikes() + "") + " Dislikes";

        //  All traces fields are updated with the properties.
        timeDay.setText(timeSinceTrace);
        textTrace.setText(textOfTrace);
        likes.setText(numberOfLikes);
        dislikes.setText(numberOfDislikes);
        String photoUrl = myTraces.get(position).getUserPhotoPath();
        if (!Utils.isStringEmpty(photoUrl)) {
            Bitmap bitmap = BitmapFactory.decodeFile(photoUrl);
            userImage.setImageBitmap(bitmap);
        }
        return vi;
    }
}
