package app.traces.common;

public interface IGPSInterface {
    public void locationChanged(double longitude, double latitude);
    public void displayGPSSettingsDialog();
}
