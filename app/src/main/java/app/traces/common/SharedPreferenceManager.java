package app.traces.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Manage shared global variables such as current user...
 */
public class SharedPreferenceManager {
    static final String TRACES_CURRENT_USER_ID = "tracesCurrentUserID";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    /**
     * Save current login user id
     * @param ctx
     * @param userId
     */
    public static void setCurrentUser(Context ctx, String userId)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(TRACES_CURRENT_USER_ID, userId);
        editor.commit();
    }

    /**
     * Get current login user ID
     * @param ctx
     * @return
     */
    public static String getCurrentUser(Context ctx)
    {
        return getSharedPreferences(ctx).getString(TRACES_CURRENT_USER_ID, "");
    }
}
