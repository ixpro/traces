package app.traces.common;

/**
 * Contains common methods that can be used in multiple files
 */
public class Utils {
    /**
     * Check if a string is null/empty or not
     * @param s The input string
     * @return true if the input is null/blank/empty
     */
    public static boolean isStringEmpty(String s) {
        if (s == null) {
            return true;
        } else {
            // trim white spaces to prevent blank input
            s = s.trim();

            if (s.isEmpty()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if there is an empty string.
     * @param stringList The input string list
     * @return true if the one of the input strings is null/blank/empty
     */
    public static boolean isThereAnEmptyString(String[] stringList) {
        for (int i = 0; i < stringList.length; i++) {
            if (isStringEmpty(stringList[i])) {
                return true;
            }
        }
        return false;
    }

    // If null, return 0, else return String s.
    public static String checkIfNotNull(String s) {
        if (s.equals("null")){
            return "0"; // For hiding mistakes to the user.
            //return "NULL" // For testing.
        }
        return s;
    }
}
