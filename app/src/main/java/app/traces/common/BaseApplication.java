package app.traces.common;

import android.app.Application;
import android.content.Context;

import com.firebase.client.Firebase;

import app.traces.model.TracesModel;

public class BaseApplication extends Application {
    private static TracesModel tracesModel;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
        tracesModel = new TracesModel();
    }

    public TracesModel getModel() {
        return BaseApplication.tracesModel;
    }

    public static Context getAppContext() {
        return BaseApplication.context;
    }
}