Traces App //TODO:
========================

## Model
	- attachObserver method
	- detachObserver method 
	- establish connection with firebase
	- on firebase update of traces => update this.traces, notify observer
	- get traces => returns this.traces array from model
	- push text trace
	- notify observer of changes (call update method)
	- traceLike(traceId)
	- traceDislike(traceId)

## Main View Controller(with the map)
	- on trace button click -> show trace type option buttons  (text, photo, recording)
	- subscribe to model updates(on new traces appearence in database) smth like model.attachObserver(this)
	- impement method for updating traces on map update() (assumed that it is called by the model)
	- change layout dynamicaly
	- onLocationChanged --> update traces in the location radius on map

## Text Trace Post Activity
	- implement getting data from text field, validation

## Traces feed Activity
	- subscribe to model
	- update() => get list of all traces around your location, put them in the list
	- filtering options? === to be discussed
	- tap on trace -> load trace ativity

## Trace Activity
	- on Like button, update this trace like counter in the model
	- on Dislike button, update this trace like counter in the model
	- get Trace comments(replies)
	- on reply Like clicked -> update reply like counter
	- on reply button, switch layout, validate input, push to model, update

## Profile Acrivity
	- get list of replies to your traces
	- get profile data from model , update fields