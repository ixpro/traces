Traces App
========================

## Main development branch:
	- dev

## Todo list:
	- https://docs.google.com/document/d/1PvbJAK5nyD5g1O5W3oZJA7vUKpDTiYdQiU2GcLlU5yE/

## Git Flow:

* (1) **git status** => see what's going on
* (2) **git add --all .** => stage all the changes, even deleted files (if any)
* (3) **git commit -m** "your message" => commit your crime
* (4) **git pull origin dev** => get latest changes from other
*  If conflict
*   (5) fix conflict in files
*   (6) repeat **1 to 3**
*  else
* (7) **git push origin dev** => Done !! It's party time